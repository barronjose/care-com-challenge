# Care.com - Coding challenge
This coding challenge is an application for weather display

## Dependencies
- Node
- Homebrew
- YARN

## Install Node
To see Node documentation of how to install it click [here](https://nodejs.org/en/)

## Install Homebrew
To see Homebrew documentation of how to install it click [here](https://brew.sh/)

## Install yarn
Once homebrew is installed use it to install yarn
```
brew install yarn
```

## Install Library dependencies
To install library dependencies run the following command on the root folder of project
```
yarn install
```

## Start Project
Run ```yarn start``` to start the project, it would be available on ```http://localhost:3000/```

## Start storybook
The project includes storybook and can de accessed via ```yarn storybook``` it would be available on ```http://localhost:6006/```

## Testing
The project includes Jest to test the react components and can be triggered with the following command
```
yarn jest
```

## Lint
To improve code quality the project includes lint using Airbnb recommended configurarion and can be run with the following command
```
yarn lint
``` 