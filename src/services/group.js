import API, { APPID } from './config';

export default {
  byIds(groupIds) {
    return API.get(`/group?id=${groupIds.join(',')}&units=metric&APPID=${APPID}`);
  },
};
