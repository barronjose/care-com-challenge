import axios from 'axios';

export const APPID = '1ff7e055534d5fb31411ab5bed550797';

export default axios.create({
  baseURL: 'https://api.openweathermap.org/data/2.5',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 10000,
});
