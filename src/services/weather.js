import API, { APPID } from './config';

export default {
  byCity(cityName) {
    return API.get(`/weather?q=${cityName}&units=metric&APPID=${APPID}`);
  },
};
