import React from 'react';
import DefaultCities from './default-cities';
import WeatherSearch from './weather-search';

const App = () => (
  <div style={{ height: '100vh', width: '100vw' }}>
    <div className="container">
      <div className="row">
        <h1>Care.com - Weather App</h1>
      </div>
      <div className="row">
        <div className="col-sm-12 col-md-4">
          <DefaultCities />
        </div>
        <div className="col-sm-12 col-md-8">
          <WeatherSearch />
        </div>
      </div>
    </div>
  </div>
);

export default App;
