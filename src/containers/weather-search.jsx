import React, { Component } from 'react';
import WeatherService from '../services/weather';
import SearchForm from '../components/SearchForm';
import NoData from '../components/NoData';
import Card from '../components/Card';

class WeatherSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      data: null,
      withError: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    this.fetchData();
  }

  onChange(query) {
    this.setState({ query });
  }

  fetchData() {
    const { query } = this.state;
    WeatherService.byCity(query)
      .then(({ data }) => this.setState({ data }))
      .then(() => {
        this.setState({ withError: false });
      })
      .catch(() => {
        this.setState({ withError: true, data: null });
      });
  }

  render() {
    const { data, withError, query } = this.state;
    return (
      <div className="container">
        <SearchForm onSubmit={this.onSubmit} onChange={this.onChange} address={query} />
        <div style={{ marginTop: 20 }}>
          { data && <Card {...data} /> }
          { withError && <NoData /> }
        </div>
      </div>
    );
  }
}

export default WeatherSearch;
