import React, { Component } from 'react';
import GroupService from '../services/group';
import Card from '../components/Card';
import { FIXED_CITIES } from '../utils/constants';

class DefaultCities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
    };
  }

  componentDidMount() {
    GroupService.byIds(FIXED_CITIES)
      .then((res) => {
        this.setState({
          cities: res.data.list,
        });
      });
  }

  render() {
    const { cities } = this.state;
    return (
      <div>
        { cities.map((city, idx) => (
          <Card key={idx} {...city} /> // eslint-disable-line react/no-array-index-key
        )) }
      </div>
    );
  }
}

export default DefaultCities;
