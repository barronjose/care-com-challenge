import React from 'react';
import { storiesOf } from '@storybook/react';

import Card from '.';
import data from '../../data/by-name.json';

storiesOf('Card', module)
  .add('default', () => <Card {...data} />);
