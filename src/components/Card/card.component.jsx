import React from 'react';
import PropTypes from 'prop-types';

const Card = ({
  name, weather, main, sys, wind, coord,
}) => (
  <div className="card" style={{ marginTop: 20 }}>
    <div className="card-body">
      <h4 className="card-title">
        {`Weather in ${name}, ${sys.country}`}
      </h4>
      <h6>{`${main.temp}°C - ${weather[0].description}`}</h6>
      <table className="table">
        <tbody>
          <tr>
            <th scope="row">Wind</th>
            <td>{`${wind.speed} km/h`}</td>
          </tr>
          <tr>
            <th scope="row">Humidity</th>
            <td>{`${main.humidity} %`}</td>
          </tr>
          <tr>
            <th scope="row">Geo coords</th>
            <td>{`[${coord.lon}, ${coord.lat}]`}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
);

Card.defaultProps = {
  name: '',
  weather: [],
  main: {},
  wind: {},
  sys: {},
  coord: {},
};

Card.propTypes = {
  name: PropTypes.string,
  weather: PropTypes.array, // eslint-disable-line react/forbid-prop-types
  main: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  wind: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  sys: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  coord: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};

export default Card;
