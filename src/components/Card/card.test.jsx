import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Card from './card.component';
import data from '../../data/by-name.json';

configure({ adapter: new Adapter() });

describe('<Card /> rendering', () => {
  it('should render Card', () => {
    const wrapper = mount(<Card {...data} />);
    expect(wrapper).toHaveLength(1);
  });
});
