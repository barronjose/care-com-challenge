import React from 'react';
import PropTypes from 'prop-types';

const SearchForm = ({ onChange, address, onSubmit }) => (
  <form onSubmit={onSubmit} className="form-inline" style={{ marginTop: 20 }}>
    <div className="form-group">
      <input
        name="query"
        id="query"
        placeholder="Enter address"
        type="text"
        className="form-control"
        value={address}
        onChange={e => onChange(e.target.value)}
      />
    </div>
    <button type="submit" className="btn btn-primary">Search</button>
  </form>
);

SearchForm.defaultProps = {
  address: '',
};

SearchForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  address: PropTypes.string,
};

export default SearchForm;
