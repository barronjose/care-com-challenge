import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SearchForm from './search-form.component';

configure({ adapter: new Adapter() });

const NO_OP = () => {};
describe('<SearchForm /> rendering', () => {
  it('should render SearchForm', () => {
    const wrapper = mount(<SearchForm onChange={NO_OP} onSubmit={NO_OP} />);
    expect(wrapper).toHaveLength(1);
  });
});
