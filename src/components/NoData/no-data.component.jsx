import React from 'react';
import PropTypes from 'prop-types';

const NoData = ({ message }) => (
  <h2>
    <i style={{ marginRight: 10 }} className="fas fa-cloud-sun-rain" />
    <span className="badge badge-warning">{message}</span>
  </h2>
);

NoData.defaultProps = {
  message: 'Sorry, the city was not found',
};

NoData.propTypes = {
  message: PropTypes.string,
};

export default NoData;
