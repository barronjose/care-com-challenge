import React from 'react';
import { storiesOf } from '@storybook/react';

import NoData from '.';

storiesOf('NoData', module)
  .add('default', () => <NoData />);
