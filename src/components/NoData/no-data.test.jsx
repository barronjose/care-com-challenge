import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import NoData from './no-data.component';

configure({ adapter: new Adapter() });

describe('<NoData /> rendering', () => {
  it('should render NoData', () => {
    const wrapper = mount(<NoData />);
    expect(wrapper).toHaveLength(1);
  });
});
