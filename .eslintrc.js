module.exports = {
  settings: {
    react: {
      version: '16.8.1',
    },
  },
  env: {
    node: true,
    browser: true,
    commonjs: true,
    es6: true,
    'jest/globals': true
  },
  extends: [
    'airbnb',
    'plugin:react/recommended',
    'plugin:jest/recommended'
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    sourceType: 'module'
  },
  plugins: ['react', 'jest'],
  rules: {
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['warn', 'single'],
    'import/no-extraneous-dependencies': [
      {
        devDependencies: [
          '.storybook/**',
          'stories/**' 
        ]
      }
    ],
  },
};
